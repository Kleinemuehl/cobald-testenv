# Requirements:

CentOS 7 vm equipped with multiple CPU cores and 2 disks: one main disk
containing OS
and an additional virtio disk containing docker images. Typically choose more
than 5GB
for size of each disk.

# Tags

Use `debug` tag like `--tags all,debug` to run assertions and get some
more debugging output.

Use `--skip-tags` with tag `cobald_monitoring` to skip setting up monitoring
using telegraf, influxdb and grafana and with tag `singularity_nodes` to
skip setting up user namespaces on each slurm execute node, which may take
some time when using on a lot of hosts or with slow connection plugins like
docker.

# Example: Setup COBalD with SLURM site and HTcondor overlay batch system

First adjust the inventory `inv.yml` to you needs, specifying the hostname
of your VM.

## using `cobald-obs-htcondor.yml`

`cobald-obs-htcondor.yml` sets up a slurm test cluster, htcondor OBS and
COBalD in one playbook.

Optionally tweak variables in the top global section of
`cobald-obs-htcondor.yml`. Run:

    ansible-playbook -i inv.yml -t all,debug -e cobald_start=False cobald-obs-htcondor.yml

COBalD and TARDIS config will be placed into `~unpriv_user/cobald`.

## using `cobald-test.yml`

Because setting up COBalD in one step with a Slurm test cluster is not a
good simulation for running against a real cluster, there is the
`cobald-test.yml` playbook which utilizes `slurm-testenv.yml` to set up the
cluster and `cobald-get-config.yml` to set up COBalD separately. Each
playbook may be run on its own, therefore `cobald-get-config` provides a good
template for running in a production environment with a real cluster.

Common variables for these playbooks are defined in
`vars/slurm-ssh-site-htcondor-obs-common.yml`. User namespaces may be disabled
by setting `use_userns=False` in this file.

# Example: Setup SLURM

First adjust inv.yml to you needs, specifying the hostname of your VM. Run:

    ansible-playbook -i inv.yml slurm-testenv.yml

Configuration goes into `/container/volumes/slurm/slurm.conf` generated
from `roles/slurm_testenv/templates/slurm.conf.j2`.

# Role documentation

* `cobald`: sets up a container with COBalD-TARDIS including site and overlay
	batch system configuration and monitoring containers for the COBalD
	pipeline and TARDIS drone status utilizing telegraf and grafana.
	[documentation](roles/cobald/README.md),
	[argument spec](roles/cobald/meta/argument_specs.yml)

* `docker`: sets up docker on el7 and builds docker images (with shell-init)
	[documentation](roles/docker/README.md)

* `singularity`: singularity installation and user namespaces setup
	[documentation](roles/singularity/README.md)

* `slurm_testenv`: slurm test cluster inside docker containers
	[documentation](roles/slurm_testenv/README.md)

* `slurm_facts`: facts and tools neccessary to use slurm with COBalD
