FROM {{ base | default('shinit:erumdata') }}

RUN yum install -y singularity &&\
    yum clean all && rm -rf /var/cache/yum

VOLUME /build

VOLUME /var/run/docker.sock

WORKDIR /build/

ENV SINGULARITY_TMPDIR="/build/cache"

ENV FILENAME="image"

CMD mkdir -p /build/build && \
        singularity build --disable-cache /build/build/${FILENAME:-image}.sif \
        /build/${FILENAME:-image}.def
