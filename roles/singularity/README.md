# role singularity

Role singularity installs singularity using yum and sets up user namespaces.
To use user namespaces `user.max_user_namespaces` sysctl variable is set to
15000 (default 0) and user and group in variable `userns_user` are enabled
to allocate 65536 UIDs and GIDs using `/etc/sub[u,g]id`.
Optionally the start UID/GID may be adjusted using `userns_map`
(default 4294836224).
Mappings for root are not created.

## variables

* `userns_user`
* `userns_map`
