- name: build htcondor docker image
  include_tasks: image.yml
  vars:
    image: htcondor:latest
    base: shinit:erumdata

# NOTE: this does also copy .swp files and trigger a configuration change.
# theres no way to avoid this besides using the synchronize module requiring
# rsync to be installed and does not permit setting permissions
- name: "copy htcondor container configuration"
  copy:
    src: "conf/{{item}}/"
    dest: "{{ htcondor_config_path }}/{{item}}/"
    mode: "u=rwX,g=rwX"
  register: htcondor_config_copy
  with_items: [ "cm", "exec", "sub", "common"]

# NOTE:
# /etc/condor/password.d needs to be present in the container
# or condor_store_cred will fail.
#
# /etc/condor/tokens.d should exist prior to invoking condor_token_create
# because else it will belong to root, even outside the container.
# This will break idempotency.
#
# Both dirs will be mounted into the container later
- name: prepare password dir
  file:
    path: "{{ htcondor_config_path }}/common/{{ item }}"
    state: directory
    mode: '700'
  loop:
    - passwords.d
    - tokens.d

- name: "parametrize htcondor configuration"
  template:
    src: "condor-parameters"
    dest: "{{ htcondor_config_path }}/common/config.d/02-parameters"
    mode: "u=rwX,g=rwX"

- name: "check if pool shared secret exists"
  stat:  # SEC_PASSWORD_FILE
    path: "{{ htcondor_config_path }}/common/passwords.d/POOL"
  register: htcondor_pool_pw

- name: "sync common files to individual containers"
  copy:
    remote_src: True
    force: True
    directory_mode: preserve
    mode: preserve
    src: "{{ htcondor_config_path }}/common/"
    dest: "{{ htcondor_config_path }}/{{item}}/"
  register: htcondor_config_sync
  with_items: [ "cm", "exec", "sub"]

- block:
  - name: "create temporary password store"
    tempfile:
      state: directory
    register: htcondor_pool_pw_tmp

  - name: "generate pool password"
    copy:
      dest: "{{htcondor_pool_pw_tmp.path}}/poolpw"
      content: "{{lookup('password','/dev/null')}}"
    no_log: True

  #- pause:

  - name: "install pool password"
    docker_container:
      name: "condor-pwgen"
      domainname: "{{htcondor_domain}}"
      image: htcondor
      state: started
      volumes:
      - "{{ htcondor_config_path }}/common/:/etc/condor/:rw"
      - "{{ htcondor_config_path }}/cm/:/tmp/cm/:rw"
      - "{{ htcondor_config_path }}/exec/:/tmp/exec/:rw"
      - "{{ htcondor_config_path }}/sub/:/tmp/sub/:rw"
      - "{{htcondor_pool_pw_tmp.path}}:/tmp/poolpw:ro"
      detach: False
#      cleanup: True
      command: >
        bash -c '
        condor_store_cred add -c -i /tmp/poolpw/poolpw &&
        for i in cm exec sub ; do
          cp -a /etc/condor/passwords.d/POOL /tmp/${i}/passwords.d/POOL ; done'
      container_default_behavior: no_defaults

  #- pause:

  - name: "remove tokens since pool password (cert) changed"
    file:  # Path from SEC_TOKEN_SYSTEM_DIRECTORY
      path: "{{ htcondor_config_path }}/{{item}}/tokens.d/condor@{{htcondor_domain}}"
      state: absent
    with_items: [ "cm", "exec", "sub" ]

  always:
  - name: "remove temporary password store"
    file:
      path: "{{htcondor_pool_pw_tmp.path}}"
      state: absent
    when: htcondor_pool_pw_tmp is defined and htcondor_pool_pw_tmp.path
  when: not htcondor_pool_pw.stat.exists

#- pause:

- name: "collect tokens to generate"
  stat:
    path: "{{ htcondor_config_path }}/{{item}}/tokens.d/condor@{{htcondor_domain}}"
  with_items: [ "cm", "exec", "sub" ]
  register: tokens_state

- debug:
    var:
      tokens_state

#- pause:

- name: "generate tokens"
  docker_container:
    name: "condor-tkgen"
    domainname: "{{htcondor_domain}}"
    image: htcondor
    state: started
    volumes:
    - "{{ htcondor_config_path }}/{{item}}/:/etc/condor/:rw"
#    - "{{ htcondor_config_path }}/common/passwords.d/:/etc/condor/passwords.d/:ro"
    detach: False
    cleanup: True
    command: >
      condor_token_create -identity condor@{{htcondor_domain}}
        {{item_authz}}
        -token /etc/condor/tokens.d/condor@{{htcondor_domain}}
    container_default_behavior: no_defaults
  vars:
    item_authz: ""
#    item_authz: "{{ (item == 'exec') | ternary('-authz ADVERTISE_STARTD
#      -authz ADVERTISE_MASTER','') }}"
  with_items: "{{tokens_state.results | rejectattr('stat.exists') |
    map(attribute='item') | list }}"

- name: "create docker network to make service discovery work"
  docker_network:
    name: "{{htcondor_docker_network | default('condor')}}"
    state: present

# TODO: reserve some address using docker_network_info and assign as aux
# address to enable cm to get a static address in order to be reachable from
# htcondor running on docker host to enable submitting jobs.

- name: "run htcondor containers"
  docker_container:
    name: "condor-{{item}}"
    hostname: "condor-{{item}}"
    domainname: "{{htcondor_domain}}"
    image: htcondor
    state: started
    detach: True
    cleanup: True
    container_default_behavior: no_defaults
    networks_cli_compatible: True
    network_mode: "{{htcondor_docker_network | default('condor')}}"
    networks:
    - name: "{{htcondor_docker_network | default('condor')}}"
      aliases: [ "condor-{{item}}.{{htcondor_domain}}" ]
    labels:
      category.htcondor: htcondor
    volumes: "{{ [ config_vol ] + htcondor_extra_volumes | default([]) }}"
  vars:
    # TODO: Not sure when bind-mounts succeed.
    # Is bind-propagation=shared necessary?
    config_vol: "{{ htcondor_config_path }}/{{item}}/:/etc/condor/:rw,shared"
  register: htcondor_run_containers
  with_items: [ "cm", "sub" ]

- name: "trigger htcondor config reload"
  command: docker exec "condor-{{item}}" condor_reconfig
  with_items: [ "cm", "sub" ]
  when: not htcondor_run_containers is changed(item) and (
    htcondor_config_sync is changed(item) or
    htcondor_config_copy is changed(item))

- name: "run htcondor exec containers"
  docker_container:
    name: "condor-{{item}}"
    hostname: "condor-{{item}}"
    domainname: "{{htcondor_domain}}"
    image: htcondor
    state: started
    detach: True
    cleanup: True
    container_default_behavior: no_defaults
    networks_cli_compatible: True
    network_mode: "{{htcondor_docker_network | default('condor')}}"
    privileged: "{{ htcondor_container_privileged | default(False) }}"
    networks:
    - name: "{{htcondor_docker_network | default('condor')}}"
      aliases: [ "condor-{{item}}.{{htcondor_domain}}" ]
    labels:
      category.htcondor: htcondor
    volumes: "{{ [ config_vol ] + htcondor_extra_volumes | default([]) }}"
  vars:
    config_vol: "{{ htcondor_config_path }}/exec/:/etc/condor/:rw"
  register: htcondor_run_exec_containers
  loop: "{{range(1, (htcondor_num_exec | default(0))+1 ) |
            map('regex_replace', '^', 'exec') | list }}"

- name: "trigger htcondor config reload on exec containers"
  command: docker exec "condor-{{item}}" condor_reconfig
  loop: "{{range(1, (htcondor_num_exec | default(0))+1 ) |
            map('regex_replace', '^', 'exec') | list }}"
  when: not htcondor_run_exec_containers is changed(item) and (
    htcondor_config_sync is changed('exec') or
    htcondor_config_copy is changed('exec'))

- name: find all running htcondor exec nodes
  command:
    cmd: 'docker ps -f name=condor-exec\* --format "{{ "{{" }}.Names}}"'
  register: htcondor_exec_container_running
  changed_when: False

- name: stop surplus htcondor exec nodes
  docker_container:
    name: "condor-exec{{item}}"
    state: absent
  loop: >-
    {{htcondor_exec_container_running.stdout_lines |
    map('regex_search', '^condor-exec([0-9]+)$', '\1') | flatten |
    select('gt', htcondor_num_exec | string) | list }}

- name: export facts about htcondor cluster to playbook scope
  set_fact:
    htcondor:
      sub:
        mounts: "{{ sub_cfg_vol + extra_volumes }}"
        config: "{{ htcondor_config_path }}/sub"
      exec:
        config: "{{ htcondor_config_path }}/exec"
    htcondor_exec_config_changed:
      "{{ htcondor_config_sync is defined and
          ( htcondor_config_sync is changed('exec') or
            htcondor_config_copy is changed('exec') ) }}"
  vars:
    sub_cfg_vol: [ '{{ htcondor_config_path }}/sub/:/etc/condor/:rw' ]
    extra_volumes: "{{ htcondor_extra_volumes | default([]) }}"
  tags: always
