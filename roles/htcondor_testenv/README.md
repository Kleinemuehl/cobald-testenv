# role htcondor\_testenv

Role htcondor\_testenv sets up a virtual htcondor cluster in docker
containers on the target host. Multiple containers may be started on the
same set of CPUs. This is impractical for real workloads but useful to
run tests using jobs executing just `sleep`.

## htcondor cgroups

HTCondor runs with cgroups inside privileged docker containers. However,
there may be issues running with cgroups enabled inside nested containers,
for example inside COBalD-TARDIS drones, so these may be disabled by removing
the `BASE_CGROUP` variables, i.e. writing `BASE_CGROUP=` in htcondor config.

## containers

There is just one container image but there are three configuration
directories being bind-mounted in a container, each corresponding to the role
of a node in the HTCondor cluster:
central manager (cm), submit node (sub), execute node (exec).

Configuration is split between common configuration for all containers images
(common) and specific configuration for the three container images. The common
configuration is copied to the individual other directories. Parametrization
is done via one template file `02-parameters`. All other configuration
files are statically copied to the configuration directories.

The pool password shared secret is generated automatically and synced to all
configuration directories as well. For each container role a dedicated
authentication token is generated, which is shared between all instances
of this role, e.g. all execute nodes.

The number of execute nodes started is defined by the variable
`htcondor_num_exec`. When changing the number of execute hosts, containers are
started or stopped on the next role run to match this requirement. This does
only affect containers named like defined in the role and does not affect
other nodes, e.g. TARDIS drones.

## exported facts

The role emits the following facts:

* `htcondor.sub/exec.config`: path to the configuration directory of a
	submit or execute node.
* `htcondor.sub.mounts`: a list of docker volume definitions necessary for a
	host to submit jobs to the cluster. This includes `/etc/condor`
	configuration directory, pointing to `htcondor.sub.config`.

`htcondor_exec_config_changed` is set to true when the config of the execute
nodes was changed in the role run.

# entrypoints

## main

The main entrypoint sets up the HTCondor cluster as described above.

## image

The image entrypoint creates the docker image containing the HTCondor daemon.
Building the image requires the entrypoint `image` of the `docker` role. The
resulting image is based on the `shinit` image. This may be changed using the
variable `base`.

# variables

* `htcondor_domain`: domain name for the cluster, default 'condor'
* `htcondor_config_path`: path where to store htcondor configuration
	directories, default `/container/volumes`.
* `htcondor_docker_network`: docker network in which cluster runs
* `htcondor_extra_volumes`: list of extra docker volumes to include in
	htcondor containers. Useful to include shared storage. Specified in docker
	volume format, e.g. `[ 'cluster-shared:/shared/:rw' ]`
* `htcondor_num_exec`: number of execute nodes to run. More execute nodes may
	be added later dynamically.
* `htcondor_container_privileged`: true if container should be run privileged,
	offering the option to create cgroups.
