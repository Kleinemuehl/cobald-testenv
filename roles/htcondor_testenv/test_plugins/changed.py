from ansible.module_utils.common._collections_compat import \
    MutableMapping, MutableSequence
from ansible import errors


# from ansible source, but modified to evaluate loops (item parameter)
def changed(result, item=None):
    ''' Test if task result yields changed '''
    if not isinstance(result, MutableMapping):
        raise errors.AnsibleFilterError(
            "The 'changed' test expects a dictionary")
    if 'changed' not in result or isinstance(item, str):
        changed = False
        if (
            'results' in result and   # some modules return a 'results' key
            isinstance(result['results'], MutableSequence) and
            isinstance(result['results'][0], MutableMapping)
        ):
            for res in result['results']:
                if not isinstance(item, str) or res.get('item') == item:
                    if res.get('changed', False):
                        changed = True
                        break
    else:
        changed = result.get('changed', False)
    return changed


class TestModule(object):
    def tests(self):
        return {'changed': changed}
