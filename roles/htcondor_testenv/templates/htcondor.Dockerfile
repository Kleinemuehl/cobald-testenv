FROM {{base | default('shinit:erumdata') }}

RUN yum install -y https://research.cs.wisc.edu/htcondor/repo/8.9/htcondor-release-current.el7.noarch.rpm && \
    yum install --nogpgcheck -y condor && \
    yum install -y less && \
    yum clean all
    # why not more cleanup?

# isn't this in the base image?
RUN yum install -y iproute bind-utils nmap-ncat net-tools && \
    yum clean all

COPY --chown=root:root 30-htcondor /etc/shell-init.d/30-htcondor

RUN chmod 755 /etc/shell-init.d/30-htcondor

