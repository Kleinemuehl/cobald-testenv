# Role Docker

Handles docker setup and building of the "shinit" images. These include
startup script and docker entrypoint management system called "shell-init".
The shinit images are based on CentOS 7 but are independent of the
distribution of the image.

## shell-init

shell-init is a simple shell script which starts all files in
`/etc/shell-init.d/` as shell background jobs and waits for their
termination. That way, used in a dockerfiles `CMD` section,
multiple services may run in parallel and the invoking shell-init script
terminates only on completion of all of these parallel tasks. When all
invoked scripts terminate and shell-init is terminated as well the termination
of all remaining processes is handled by the process reaping init replacement
of docker or singularity.

If the shell-init process receives a maskable signal i.e. `SIGHUP`, `SIGINT`,
`SIGQUIT`, `SIGTERM`, `SIGUSR1` or `SIGUSR2` it invokes a bash trap and tries
to send this signal to all its child processes and ensures a proper shutdown
of multiple services. That way both `docker stop` works and a container stops
properly if its services terminate (e.g. upon service failure or service
desired container shutdown, like a drone shutdown).

Unlike sysvinit services placed in `/etc/init.d/`, shell-init scripts
in the `/etc/shell-init.d/` directory receive no command line arguments when
invoked. Termination works in the way that the scripts started receive a
termination process signal like SIGTERM
so scripts have to implement traps to catch this and kill
their children. Another option is to have these scripts use `exec` to
start their services which makes them be replaced by the service process
they execute.

Being able to place additional scripts in `/etc/shell-init.d/` allows for
multiple build stages or docker image layers to extend functionality and
start independent but interoperating services without having to configure
these together in one stage.

Putting containers into common namespaces like in so called pods used
by podman or kubernetes would solve this issue as well, but this would
still isolate the containers filesystems and make usage of unix sockets
difficult, despite this could be solved by sharing volumes between
containers. Docker also features the ability to share namespaces in
multiple containers but configuration would be much more difficult.

Alternatives to shell-init would have been to use systemd which is difficult
because of its excessive use of cgroups and missing monolithic architecture
which tries to handle much more than process starting. sysvinit and other init
systems would have been another option but usually these don't terminate when
their services stop. Supervisord and other commonly used init replacements
for docker containers usually require to be configured by a single
configuration file or are not included in CentOS and EPEL repositories or
are disadvantageous in other ways.

## entrypoint script

The entrypoint script `entrypoint.sh` included in shinit containers is
intended to be run in the `ENTRYPOINT` section instead of the `CMD` section
of a Dockerfile and thereby running on every container start, even if a custom
command is defined at the container defintion. This enables prepartion or
cleanup tasks, which always need to run, e.g. setting permissions or installing
software delivered in volumes, being only available at container run time but
not a at container build time. The script operates similar to shell-init by
starting all shell scripts at the directory /usr/local/lib/entrypoints.d/,
but running these scripts in series and not as background tasks.

## docker setup

The `install` entrypoint, also called by `main` when `docker_install` is true,
sets up docker by using the official docker rpm
distribution repository. The docker daemon will be enabled to start at
system boot.

Most docker and container related files are moved to
`docker_containerfs_mountpoint` where
a separate disk is mounted partitioned with a a new xfs filesystem on
disk `docker_containerfs_disk` in partition
`docker_containerfs_disk_part`. For more information see [below](#variables).
Per default, this filesystem will be created on `/dev/vdb1` and mounted at
`/container`.

The former `/var/lib/docker` directory, where docker keeps its runtime
files like container images and volumes, resides in `/container/docker/`.
Image build files (`image` entrypoint) should be located at directories in
`/container/docker-images/`, docker volume bind mounts in
`/container/volumes/`.

The unprivileged user given by the `unpriv_user` variable will be added to
the group docker. Note, that this might mean that this user may escalate
its privileges to root.

## shinit image

The shinit image is a common base for containers used as test cluster or
for COBalD-TARDIS. It is useful to create users utilizing the
`docker_shinit_users` parameter to maintain the same user and group ids
across different containers having a common filesystem. The image contains
shell-init and the entrypoint script. Also, the timezone is set.

## image building

The role provides a facility for easy building of docker images with
dependency management. That means that docker images (re-)built in one
playbook run will also trigger a new build of images in the same playbook
which depend on it. For this to work, the variable `base` in the role
invocation of the depending image has to be specified, matching the
`FROM` section in the Dockerfile. If no value for `base` is given, no
dependency management will be done. If the base image has not been specified
in the same playbook run before, but `base` is specified, a warning will
be issued when running with the `debug` tag.

The role cares for building as well as uploading the required files to the
remote host and tagging the resulting image. For the latter, the tag of
the resulting image has to be specified in the `image` variable which must
be in the form `image:tag`. For the former the content of the Dockerfile
must be given as variable `dockerfile`. This must not be a file path but
the content of the Dockerfile to facilitate templating of the file.
Files to be included inside the docker image can be defined using the
`files` variable containing a list of dicts. These will be put, together with
the Dockerfile, inside a directory like `/container/docker-images/image-tag`
where `docker build` will have its working directory. Changes of the
Dockerfile and the provided files will trigger a new build of the docker
image.

The `files` variable uses a list of dicts as specification for the files to
be uploaded. Each item must contain a value for the `file:` key to provide
the target file name. Per default this value is also used to look up the
content of source file to be provided. This may be overridden by using
`content` to provide the files content inside a variable, which can be
useful to dynamically create files, e.g. from templates.

Additional variables may be provided to be used inside the templated
Dockerfile, when the 'template' lookup is used to fetch the Dockerfile
content. If a variable `args` is provided, its content is used as environment
variables on the `docker build` run.

## variables

* `docker_shinit_image`: name and tag of the shinit image, defaults
	to `shinit:centos7`
* `docker_shinit_users`: users to be created in the shinit image. Attributes:
	* `name` user name
	* `dir`: optional home directory, defaults to `/var/lib/name`
* `docker_shinit_timezone`: container timezone. Default is UTC
* `unpriv_user`: unprivileged user to put in docker group

### variable on install (and main) entrypoint

* `docker_containerfs_disk`, `docker_containerfs_disk_part` define the disk
	and partition where the filesystem containing docker related files will
	be created. The partition variable will be generated as the first
	partition on the disk, if it is not given and the disk is either a `sd*`,
	`hd*` or `vd*` device directly at `/dev/` or located in `/dev/disk/by-*`.
	Otherwise both have to be defined.
* `docker_containerfs_mountpoint` is the mountpoint where the disk containing
	container stuff is mounted, usually `/container`.

### variable on image entrypoint

These should not be defined globally but instead provided directly on the
`include_tasks` task.

* `docker_builddir`: directory where to put files necessary for build.
* `image`: image name and tag
* `base`: image name and tag for the image to depend on
* `dockerfile`: content of the Dockerfile (not a file name!)
* `files`: files required by image build, list of dicts with keys as follows:
	* `file`: name of the file name. Also provides name for source lookup, if
		`content` is not given.
	* `content`: content of the file
* `args`: environment variable arguments to the docker build task. Note this
	has to be given as task module argument and not in the scope of the
	task definition itself, which would override module arguments.
* `do_build`: either true, false or `force`, default true. Disable building
	image setting `do_build = False`. Force building using `do_build = force`

### facts

* `docker_build_img_chg`: dict of booleans, true if a image given as key
	changed

### internal variables

* `_docker_build_dockerfile`, `_docker_build_copy_files`,
	`_docker_build_image`: registered result of the dockerfile copy, file copy
	and docker build commands for a specific image
* `_docker_build_images`: list of images already built. Used to avoid building
	a image twice in the same playbook altering the changed state erroneously.

## Entrypoints

### main

The `main` entrypoint sets up docker as described under
[docker setup](#docker_setup) above.

Other entrypoints usually depend on main being run before, since docker must
be installed and there must be a `/container/docker-images/` directory. This
may also be achieved by other means like custom tasks, therefore this
entrypoint is not enforced or asserted anywhere.

### image-shinit

Builds the `shinit` base image.

### image

Builds an arbitrary image defined by variables described
[above](#variable_on_image_entrypoint).

## Tags

* `debug`: add this tag for more debugging output and check of assertions

## Example

```
- include_role:
    name: docker
    tasks_from: image
  vars:
    image: foo:bar
    base: shinit:centos7
    dockerfile: "{{ lookup('file', 'foo.Dockerfile') }}"
    files:
    - file: foo
      content: "{{ lookup('template', 'foo.j2') }}"
```
