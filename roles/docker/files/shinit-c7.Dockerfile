FROM docker.io/library/centos:7

RUN mkdir -p /usr/local/lib/entrypoints.d/ /etc/shell-init.d

COPY --chown=root:root entrypoint.sh /usr/local/sbin/entrypoint.sh
COPY --chown=root:root shell-init /usr/local/sbin/shell-init

RUN chmod 755 /usr/local/sbin/entrypoint.sh && \
    chmod 755 /usr/local/sbin/shell-init

ENTRYPOINT [ "/usr/local/sbin/entrypoint.sh" ]

CMD [ "/usr/local/sbin/shell-init" ]

ARG addusers
RUN function au { [ -z "$1" ] && return ; \
        useradd -d $2 -m --no-log-init --system $1 -s ${3:-/sbin/nologin} ;};\
    echo "${addusers}" | tr ',' '\n' | while read i ; do au $i ; done

RUN yum install -y less iproute bind-utils nmap-ncat net-tools && \
    yum install -y epel-release && \
    yum clean all && rm -rf /var/cache/yum

ARG TZ=UTC
RUN ln -snf /usr/share/zoneinfo/${TZ} /etc/localtime && \
    echo ${TZ} > /etc/timezone
