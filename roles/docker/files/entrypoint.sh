#!/usr/bin/env bash
set -e

for i in /usr/local/lib/entrypoints.d/* ; do
    [ -f $i ] && /bin/sh $i || break
done

exec "${@:-/bin/bash}"
