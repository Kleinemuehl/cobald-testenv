- name: build docker images for slurm
  include_role:
    name: docker
    tasks_from: image
  loop:
  - tag: base
    base: shinit:erumdata  # built by docker role
    dockerfile: "{{lookup('file', 'slurm-base.Dockerfile')}}"
    files:
    - file: entry-munge.sh
    - file: start-scripts/10-munge
  - tag: slurmctld
    dockerfile: "{{ lookup('file', 'slurmctld.Dockerfile') }}"
    files:
    - file: start-scripts/20-slurmctld
  - tag: slurmd
    dockerfile: "{{ lookup('file', 'slurmd.Dockerfile') }}"
    files:
    - file: start-scripts/30-slurmd
  vars:
    image: "{{ slurm_base_image_prefix }}:{{ img.tag }}"
    base: "{{ img.base | default((slurm_base_image_prefix ) + ':base') }}"
    dockerfile: "{{ img.dockerfile }}"
    files: "{{ img.files | default(omit) }}"
    args: "{{ img.args | default(omit) }}"
  loop_control:
    loop_var: img
    label: "{{ img.tag }}"

- name: create munge user
  user:
    name: munge
    system: True
    comment: "Runs Uid 'N' Gid Emporium"
    home: /var/run/munge
    shell: /sbin/nologin
    create_home: True
  when: slurm_munge_user is undefined

# We need to create the parent dir first.
# Otherwise everything will be owned by the munge user.
# Also: mode 777 is preparation because unprivileged users will want to store
# volumes in this dir too.
# PROBLEM: This makes /etc/muge world-accessible
#- name: Create parent dirs
#  file:
#    path: "{{ item | split | first }}"
#    state: directory
#    mode: '777'
#  loop: "{{slurm_munge_key_locations | map('dirname') | list }}"

# Volumes will go here.
# Unprivileged users will want to store some too.
- name: Create volumes dir
  file:
    path: "{{ slurm_etc_dir }}"
    state: directory
    mode: '777'

- name: create munge key directories
  file:
    path: "{{item}}"
    state: directory
    owner: "{{ slurm_munge_user | default('munge') }}"
    group: "{{ slurm_munge_group | default('munge') }}"
    mode: u=rwx,g=,o=
  loop: "{{slurm_munge_key_locations | map('dirname') | list }}"

- name: generate munge key
  shell:
    cmd: "umask 177 &&
          dd if=/dev/urandom bs=1 count=1024 > {{ key_location }}"
    creates:  "{{ key_location }}"
  vars:
    key_location: "{{ slurm_munge_key_locations | first }}"

- name: set access permissions on munge key
  file:
    path: "{{ slurm_munge_key_locations | first }}"
    owner: "{{ slurm_munge_user | default('munge') }}"
    group: "{{ slurm_munge_group | default('munge') }}"
    mode: u=rw,g=,o=

- name: copy munge key to docker containers
  copy:
    remote_src: True
    force: true
    mode: preserve
    src: "{{ slurm_munge_key_locations | first }}"
    dest: "{{ item }}"
  loop: "{{ slurm_munge_key_locations[1:] }}"

- name: make slurm directory
  file:
    path: "{{ slurm_config_dir }}"
    state: directory

- name: "create docker network to make service discovery work"
  docker_network:
    name: "{{ slurm_docker_network }}"
    state: present
  register: _slurm_network_data
  tags: slurm-config

- name: ssh submit node
  import_tasks:
    file: submit_ssh.yml
  when: slurm_sshsub

- name: upload slurm config
  template:
    force: true
    src: "{{item}}.j2"
    dest: "{{ slurm_config_dir }}/{{item}}"
  loop:
    - slurm.conf
    - cgroup.conf
  vars:
    _alloc_nodes_default:
    - name: "{{ slurm_container_prefix+'-submit1' }}"
    _alloc_nodes_host:
    - name: "{{ inventory_hostname }}"
      addr: "{{ _slurm_network_data.network.IPAM.Config[0].Gateway }}"
    alloc_nodes: "{{ _alloc_nodes_default +
        _slurm_sshsub_alloc_nodes | default([]) +
        slurm_hostsystem_cluster_access | ternary( _alloc_nodes_host, []) +
        slurm_extra_nodes | default([]) }}"
    partitions: "{{ slurm_partitions | default([]) }}"
  notify: reconfigure slurm
  tags: slurm-config

- name: run slurm docker containers
  debugger: always
  docker_container:
    name: "{{ slurm_container_prefix }}-{{ item.machine }}"
    hostname: "{{ slurm_container_prefix }}-{{ item.machine }}"
    domainname: "{{ slurm_domain }}"
    volumes: "{{ _slurm_config_volumes + slurm_extra_volumes | default([]) +
                 ( item.extra_mounts | default([]) ) }}"
    ports: "{{ item.exposed_ports | default([]) }}"
    networks_cli_compatible: True
    network_mode: "{{ slurm_docker_network }}"
    networks:
    - name: "{{ slurm_docker_network }}"
      aliases: "{{ item.aliases | default(omit) }}"
    labels:
      category.slurm: slurm
    env:
      slurmctlduser: "{{ slurm_slurmctld_user }}"
    image: "{{ item.image }}"
    state: started
    container_default_behavior: no_defaults
    detach: True
    cleanup: True
    privileged: "{{ slurm_container_privileged | bool }}"
    interactive: True
  vars:  # see vars/main.yml
    _slurm_nodes_all: "{{ _slurm_nodes_exec + _slurm_nodes_std +
      _slurm_nodes_ssh | default([]) }}"
  loop: "{{ _slurm_nodes_all }}"
  loop_control:
    label: "{{slurm_container_prefix}}-{{ item.machine }}"
  tags: slurm-config

- name: configure host system to integrate into slurm cluster
  import_tasks: host-config.yml
  when: slurm_hostsystem_cluster_access | default(False)

- name: export facts about slurm cluster to playbook scope
  set_fact:
    slurm:
      user: "{{slurm_slurmctld_user}}"
      slurmctld_user: "{{slurm_slurmctld_user}}"
      domain: "{{slurm_domain}}"
      base_image: "slurm:base"
      mounts: "{{ _slurm_config_volumes + slurm_extra_volumes | default([]) }}"
      network: "{{ slurm_docker_network }}"
      role: slurm_testenv
  tags: always
