# role slurm\_testenv

Role slurm\_testenv sets up a virtual slurm cluster in docker containers
on the
target host. Multiple containers may be started on the same set of CPUs.
This is impractical for real workloads but useful to run tests using
jobs executing just `sleep`.

Host integration is an option, so one can look at the cluster state from
the docker host environment.

A submit node accessible via SSH may also be created.

As select type `select/cons_res` with `CR_CORE` is used to let jobs
allocate nodes partially. There is no accounting configured.

Besides the `main` role entrypoint there is `ssh_addkey` which adds a SSH key
to the ssh submit node. For parameters of `ssh_addkey` see
[below](#SSH_submit_node).

## slurm, docker and cgroups

In some rare scenarios, cgroups have to be deactivated in the slurm config,
because nesting cgroups is a feature of newer kernels and older software
lacks transparent support for nesting them. Since creating cgroups requires
elevated privileges, running slurm in privileged containers enables cgroups.
Still this may break some features. If cgroups are disabled, `linuxproc`
process tracking is enabled in slurm configuration.
Otherwise `ProctrackType` is set to `cgroup` and `task/affinity,task/cgroup`
are used as task plugins.

## host integration

Host integration enables the docker host to reach the cluster running inside
docker containers.

If host integration is configured, the `sinfo` and
`scontrol` work but starting jobs, i.e. `srun` and `sbatch` do not.
This is because only nodes defined in the slurm configuration can submit
jobs and these hosts need to be reverse resolvable by its IP address to
a hostname defined in the slurm configuration file. This is hard to
implement because docker does not provide an easy way for configuring DNS,
instead relying on its network feature. Adding a host to `/etc/hosts` does
not work since this only provides forward but no reverse resolution; adding
an `aux_address` to the `ipam_config` section of the network configuration
works neither, because the gateway, where the host system is located, is an
allocated address and therefore marked as used.

For the host to interact with the cluster the package `slurm` is installed
from the official CentOS7 repositories. Additionally `slurm-doc` is installed
for convenience. Running slurmd is not necessary because the host is not a
execute node where jobs run. As usual for slurm installations, the same
config is running in the container nodes as well as on the host system,
so the host systems configuration file is just a symlink to the file of the
dockerized cluster.

To run slurm a system user 'slurm' is necessary and a entry for the slurm
controller is added to `/etc/hosts` of the host system because docker has
no option to expose its DNS service to the host system. The munge service
required for authentication is started and enabled at system boot. There is
no need to copy the munge keys, because these are usually placed in
/etc/munge anyway.

Enable by setting `slurm_hostsystem_cluster_access` to true.

## SSH submit node

SSH submit nodes offer a ssh server to log in and remotely start jobs using
`srun` and `sbatch`. Authentication should happen through SSH keys using
the [`ssh_addkey` entrypoint](#entrypoint_ssh_addkey). To offer a secure
way to connect initially the ssh host key is exported as fact
`slurm_sshsub_hostkey`.

### entrypoint `ssh_addkey`

The `ssh_addkey` entrypoint requires two variables: `user` and `keypath`.
`user` is the user to whom the ssh key generated should be added. `keypath`
is the file path where the ssh key will be written to. A public key will
automatically generated by adding `.pub` to `keypath`.

## users

slurmctld may be run as root or as unprivileged user according to the
`slurm_slurmctld_user` variable. slurmd will always run as root since it
needs to launch jobs running as different user, making setuid permissions
necessary. It is possible to configure a user for the munge service using
`slurm_munge_user` to avoid creating a new user specifically for storing
keys, but per default one is created. Despite this, the munge key in
`/container/volumes/munge` may belong to a completely different user, even
one which already exists and is unrelated to slurm, since UIDs in containers
differ from the ones in the host system. This is a flaw in docker, which can
not be solved unless using fakeroot/user namespaces or matching host and
container user ids.

## docker containers

Docker containers defined by the slurm\_testenv role get the container label
`category.slurm=slurm`.
A hostname, matching the container name, and a domainname are defined.
There are multiple bind mounts used to include configuration and keys, but
per default there is no shared volume between containers, which may be added
by creating an entry in the `slurm_extra_volumes` list.

All slurm hosts must get allocated in one network, defined by
`slurm_docker_network`, which is also exported via the `slurm.network` fact.
This is because addressing and reverse lookup in all slurm containers works
via hostnames distributed using DNS of dockers integrated nameserver, which
uses the container names inside the docker networks for its ressource
records. For more information about issues with docker networking and slurm
see the [section host integration above](#host_integration).

There will be one container hosting the slurmctld (`slurm-ctl`), one machine
for submitting jobs (`slurm-submit`) and `slurm_num_nodes` machines as
execute nodes (`slurm-execN`). Additionally there may be a ssh submit node
(`slurm-sshsub`).

## Variables

* `slurm_munge_user`, `slurm_munge_group`: user and group who owns the munge
	key for the slurm cluster. If undefined a default user 'munge' will be
	created automatically, so usually this should not be changed.
* `slurm_image_prefix`: name of the docker images which this role will create.
	Each image will get a different tag with this image name.
* `slurm_munge_key_locations`: list of filesystem locations where the munge
	key should be placed. The key will be generated in the first directory of
	this list with forced permissions and owner/group and copied to the other
	places in the list later, where permissions will not be changed after
	copy. This variable defaults to `slurm_munge_key_locations_default`,
	which is `/etc/munge/munge.key` and `/container/volumes/munge/munge.key`.
* `slurm_docker_network`: network to be used to connect containers of the slurm
	cluster. Will be created if it does not exist yet.
* `slurm_container_prefix`: prefix of the container names. When set to `slurm`
	(the default value) containers will be named `slurm-exec1`.
* `slurm_domain`: default domain name of slurm containers.
* `slurm_slurmctld_user`: user which will run slurmctld. This should not be
	the root user.
* `slurm_container_privileged`: for job management slurm should either use
	cgroups, which offer a better control over processes started by a job
	or use the linux process table (`linuxproc`). Running the container in
	privileged mode enables the slurm to use cgroups for process tracking.
* `slurm_extra_nodes`: extra nodes to allocate in the cluster, especially
	for submission jobs. COBalD-TARDIS gets access to the cluster as site
	this way. The nodes have to be defined as dict with the keys:
	`name` (`NodeName`, mandatory), `hostname` (`NodeHostname`),
	`addr` (`NodeAddr`).
* `slurm_num_nodes`: number of execute nodes
* `slurm_exec_node_cores`: cpu cores of exec nodes in slurm config, defaults
	to total cores of container host
* `slurm_exec_node_mem` memory of exec nodes in slurm config, defaults
	to total memory of container host
* `slurm_exec_node_disk`: disk size of execute node in slurm config, default 0
* `slurm_partitions`: additional partitions, i.e. groups of nodes, to create.
	Useful for creating dynamic overlay partitions with a fixed number of
	nodes to be used as drones by COBalD-TARDIS. Nodes defined this way
	will not be created as containers but only allocated, i.e. defined, in
	the slurm config file. Defined as list of dicts with the keys:
	* `nodeprefix`: prefix of the nodes, i.e. defined as `foo` nodes will
		be named `foo1` to `foo100`
	* `num_nodes`: number of nodes to allocate
	* `node_cores`, `node_mem`, `node_disk`: ressources of each node
	* `port`: port where slurmd runs on the nodes
	* `initstate`: initial state a node has on slurmctld startup, e.g.
		`UNKNOWN` or `FUTURE`.
* `slurm_extra_volumes`: List of additional volumes a slurm node should have,
	in docker volume format (`host-path:destination-path:rw`). This
	may include shared volumes between nodes.
* `slurm_hostsystem_cluster_access`: Enable host integration (boolean).
* `slurm_sshsub`: Enable `slurm-sshsub` submit node (boolean).
* `slurm_sshsub_keytype`: type of ssh host keys (default ed25519).
* `slurm_sshsub_dir`: location of all ssh data (keys and configuration),
	defaults to `{{ slurm_etc_dir }}/slurm-sshsub`

### internal

* `_slurm_nodes_exec`, `_slurm_nodes_std`, `_slurm_nodes_ssh`:
	definition of exec nodes, other nodes, e.g the controller and submit nodes,
	and ssh submit nodes respectivly. Contains
	information about the image and ports exposed.
* `_slurm_sshsub_alloc_nodes` allocation parameters of ssh submit node for
	slurm configuration.
* `_slurm_network_data`: job result from docker network creation or fetching
	network data. Used to evaluate docker network data, e.g. to find ip
	addresses.
* `_slurm_config_volumes`: volumes required to run slurm, i.e. config and keys.
* `_slurm_sshsub_keygen`, `_slurm_sshsub_hostkey`: raw data from key
	generation tasks.
* `_slurm_sshsub_authkeys_dir`: provides `slurm_sshsub_keydir`

## Facts

The role exports multiple facts to be used by other roles or tasks. All of
these are stored as dict in the slurm variable:

* `slurm.user`,`slurm.slurmctld_user`: user under which slurmctld runs. This
	user owns the slurm configuration file.
* `slurm.domain`: domain name of machines in the slurm cluster
* `slurm.base_image`: base image containing the slurm software (like srun
	etc.) but no slurmd
* `slurm.mounts`: volumes to be used on a slurm node. This also includes
	shared volumes defined via `slurm_extra_volumes`.
* `slurm.network`: docker network which slurm cluster users
* `slurm.role`: name of this role to enable finding correct role for
	`ssh_addkey` entrypoint.
* `slurm_sshsub_keydir`: ssh client key directory for the ssh submit node
* `slurm_sshsub_hostkey`: host key of the submit node
