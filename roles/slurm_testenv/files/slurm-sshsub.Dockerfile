FROM slurm:base

RUN yum install -y openssh-server openssh-clients && \
    yum clean all && rm -rf /var/cache/yum

COPY --chown=root:root start-scripts/31-sshd /etc/shell-init.d/31-sshd
RUN chmod 755 /etc/shell-init.d/31-sshd

COPY --chown=root:root sshd_config /etc/ssh/sshd_config
RUN chmod 640 /etc/ssh/sshd_config

RUN mkdir /etc/ssh/authorized_keys/
