FROM slurm:base

RUN yum install -y slurm-slurmctld && \
    yum clean all && rm -rf /var/cache/yum

COPY start-scripts/20-slurmctld /etc/shell-init.d/20-slurmctld
RUN chmod 755 /etc/shell-init.d/20-slurmctld

ENV SLURMCTLD_LOG_PATH="/var/log/slurm/slurmctld.log"
ENV SLURMD_LOG_PATH="/var/log/slurm/slurmd.log"
ENV SLURM_SCHED_LOG_PATH="/var/log/slurm/slurmsched.log"
