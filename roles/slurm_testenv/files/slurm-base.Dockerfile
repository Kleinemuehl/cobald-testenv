FROM shinit:erumdata

RUN yum install -y epel-release && \
    yum install -y slurm && \
    yum clean all && rm -rf /var/cache/yum

COPY --chown=root:root entry-munge.sh /usr/local/lib/entrypoints.d/10-munge.sh
COPY --chown=root:root start-scripts/10-munge /etc/shell-init.d/10-munge

RUN chmod 755 /usr/local/lib/entrypoints.d/10-munge.sh && \
    chmod 755 /etc/shell-init.d/10-munge

ARG slurmctlduser=slurm
ENV slurmctlduser=${slurmctlduser}

RUN useradd -d /var/lib/slurm -m --no-log-init --system $slurmctlduser ;\
    slurm-setuser -u $slurmctlduser -g $slurmctlduser -y
