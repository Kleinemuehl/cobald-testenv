#!/usr/bin/env bash
set -e

if [ -f "/etc/munge/munge.key" ] ; then
	chown munge:munge /etc/munge/munge.key
	chmod 600 /etc/munge/munge.key
fi
