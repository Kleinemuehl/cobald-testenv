FROM slurm:base

RUN yum install -y slurm-slurmd && \
    yum clean all && rm -rf /var/cache/yum

COPY start-scripts/30-slurmd /etc/shell-init.d/30-slurmd
RUN chmod 755 /etc/shell-init.d/30-slurmd

ENV SLURMCTLD_LOG_PATH="/var/log/slurm/slurmctld.log"
ENV SLURMD_LOG_PATH="/var/log/slurm/slurmd.log"
ENV SLURM_SCHED_LOG_PATH="/var/log/slurm/slurmsched.log"

RUN yum install -y singularity && \
    yum clean all && rm -rf /var/cache/yum
