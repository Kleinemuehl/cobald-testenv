#!/usr/bin/env python
import requests
from ansible.module_utils.basic import AnsibleModule

DOCUMENTATION = r'''
---
module: influx2_bucket
short_description: create bucket in influxdb2
description: create bucket in influxdb2
notes:
    - just works with influxdb version 2
    - does not remove buckets
    - no way to configure data retention

options:
    base:
        description: URL for path, e.g. `https://localhost:8086`
        type: str
        required: True
    org:
        description: influxdb2 organisation
        type: str
        required: True
    auth_token:
        description: influxdb2 authentication token
        type: str
        required: True
    name:
        description: name of the bucket
        type: str
        required: True
    force:
        description: force creation even if bucket already exists
            (adds a new one)
        type: bool
        required: False
        default: False
author:
    - Thorsten M. (@thoto)
'''

EXAMPLES = r'''
- name: "fetch auth token"
  raw: influx auth list --user my-user --hide-headers | cut -f 3
  register: influx_token_fetch
  delegate_to: ed-influxdb-2

- name: "create bucket"
  influx_bucket:
    base: "http://localhost:8086"
    org: "my-org"
    token: "{{influx_token_fetch.stdout_lines[0]}}"
    name: "bucky"
'''


def get_org_id(base, org_name, h):
    ro = requests.get("{base}/api/v2/orgs".format(base=base), headers=h,
                      json={"org": org_name})
    ro.raise_for_status()
    org_id = [o["id"] for o in ro.json()["orgs"] if o["name"] == org_name]
    return org_id[0]


class Bucket:
    def __init__(self, base, h, org, description, name):
        self.base = base
        self.h = h
        self.org_id = get_org_id(base, org, h)
        self.description = description
        self.name = name

        self.result = None
        self.f = None

    def check(self):
        ra = requests.get("{base}/api/v2/buckets".format(
                            base=self.base),
                          params={"orgID": self.org_id, "name": self.name},
                          headers=self.h)
        if ra.status_code == 404:
            # orgID + name -> 404 on empty set. Just name -> 200 but buckets=[]
            x = []
        else:
            ra.raise_for_status()
            x = [i for i in ra.json()["buckets"]
                 if self.name == i["name"] and i["orgID"] == self.org_id]
        assert(len(x) == 1 or len(x) == 0)

        update = None
        if len(x) == 0:  # create
            self.result = None
            self.f = lambda: self._create({
                "orgID": self.org_id,
                "description": self.description if self.description else None,
                "name": self.name,
                "retentionRules": []
            })
        else:
            self.result = x[0]
            if self.description == x[0].get("description", ""):
                return False  # everything matches -> no change needed
            else:
                self.result = x[0]
                update = {"id": x[0]["id"],
                          "description": self.description}
            self.f = lambda: self._update(**update)
        return True

    def run(self):
        if not self.f:
            self.check()
        self.f()

    def _update(self, id, description):
        ra = requests.patch(
               "{base}/api/v2/buckets/{id}".format(
                   base=self.base, id=id),
               headers=self.h, json={"description": description})
        ra.raise_for_status()
        return ra

    def _create(self, data):
        ra = requests.post("{base}/api/v2/buckets".format(base=self.base),
                           headers=self.h, json=data)
        ra.raise_for_status()
        self.result = ra.json()
        return ra


if __name__ == "__main__":
    result = dict(changed=False, message="")
    module = AnsibleModule(
        argument_spec=dict(
            base=dict(type="str", required=True),
            org=dict(type="str", required=True),
            auth_token=dict(type="str", required=True),
            name=dict(type="str", required=True),
            description=dict(type="str", default=""),
            force=dict(type="bool", default=False),
        ),
        supports_check_mode=True
    )
    h = {"Authorization": "Token {token}".format(
        token=module.params["auth_token"])}
    b = Bucket(module.params["base"], h, org=module.params["org"],
               description=module.params["description"],
               name=module.params["name"])

    changed = b.check()
    if b.result:
        result['bucket_id'] = b.result["id"]
    result['changed'] = changed

    if module.check_mode:
        module.exit_json(**result)

    if changed or module.params["force"]:
        b.run()
        result['bucket_id'] = b.result["id"]
    module.exit_json(**result)
