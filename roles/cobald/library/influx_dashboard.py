#!/usr/bin/env python
import json
import requests
from ansible.module_utils.basic import AnsibleModule

DOCUMENTATION = r'''
---
module: influx2_dashboard
short_description: create dashboard in influxdb2
description: create dashboard in influxdb2
notes:
    - just works with influxdb version 2
    - does not create dashboard description
    - does not update dashboards
    - just creates a dashboard if it does not exist.

options:
    base:
        description: URL for path, e.g. `https://localhost:8086`
        type: str
        required: True
    org:
        description: influxdb2 organisation
        type: str
        required: True
    auth_token:
        description: influxdb2 authentication token
        type: str
        required: True
    data:
        description: exported dashboard json file
        type: json
        required: True
    force:
        description: force creation even if dashboard already exists
            (adds a new one)
        type: bool
        required: False
        default: False
author:
    - Thorsten M. (@thoto)
'''

EXAMPLES = r'''
- name: "fetch auth token"
  raw: influx auth list --user my-user --hide-headers | cut -f 3
  register: influx_token_fetch
  delegate_to: ed-influxdb-2

- name: "create dashboard"
  influx_dashboard:
    base: "http://localhost:8086"
    org: "my-org"
    auth_token: "{{influx_token_fetch.stdout_lines[0]}}"
    data: "{{lookup('file', 'influxdb-dashboard-cobald.json')}}"
'''


def parse_dashboard_template(data):
    j = json.loads(data)

    return {
        'name': j["content"]["data"]["attributes"]["name"],
        'cells': {i["relationships"]["view"]["data"]["id"]: i["attributes"]
                  for i in j["content"]["included"] if i["type"] == "cell"},
        'views': {i["id"]: i["attributes"]
                  for i in j["content"]["included"] if i["type"] == "view"},
    }


def get_auth(base, org_name, token):
    h = {"Authorization": "Token {token}".format(token=token)}

    rm = requests.get("{base}/api/v2/me".format(base=base), headers=h)
    rm.raise_for_status()
    # me_name = rm.json()["name"]
    me = rm.json()["id"]

    ro = requests.get("{base}/api/v2/orgs".format(base=base), headers=h,
                      json={"userID": me})
    ro.raise_for_status()
    org_id = [o["id"] for o in ro.json()["orgs"] if o["name"] == org_name]

    return {"org_id": org_id[0], "org_name": org_name, "uid": me, "h": h}


def check(base, auth, dashboard):
    h = auth["h"]
    rd = requests.get("{base}/api/v2/dashboards".format(base=base), headers=h)
    rd.raise_for_status()
    x = [i for i in rd.json()["dashboards"]
         if i["name"] == dashboard["name"] and i["orgID"] == auth["org_id"]]
    return len(x) == 0, x


def create(base, auth, dashboard):
    h = auth["h"]

    # create dashboard
    rd = requests.post("{base}/api/v2/dashboards".format(base=base), headers=h,
                       json={"orgID": auth["org_id"],
                             "name": dashboard["name"]})
    rd.raise_for_status()
    dash_id = rd.json()["id"]

    for k, v in dashboard["cells"].items():
        # create cells in dashboard
        rc = requests.post(
            "{base}/api/v2/dashboards/{dash_id}/cells".format(
                base=base, dash_id=dash_id),
            headers=h, json=v)
        rc.raise_for_status()
        cell_id = rc.json()["id"]

        # create view of cell in dashboard
        rv = requests.patch(
             "{base}/api/v2/dashboards/{dash_id}/cells/{cell_id}/view".format(
                base=base, dash_id=dash_id, cell_id=cell_id),
             headers=h, json=dashboard["views"][k])
        rv.raise_for_status()


if __name__ == "__main__":
    result = dict(changed=False, message="")
    module = AnsibleModule(
        argument_spec=dict(
            base=dict(type="str", required=True),
            org=dict(type="str", required=True),
            auth_token=dict(type="str", required=True),
            data=dict(type="json", required=True),
            force=dict(type="bool", default=False),
        ),
        supports_check_mode=True
    )
    dashboard = parse_dashboard_template(module.params["data"])
    auth = get_auth(module.params["base"], module.params["org"],
                    module.params["auth_token"])
    changed, x = check(module.params["base"], auth, dashboard)

    result['changed'] = changed

    if module.check_mode:
        module.exit_json(**result)

    if changed or module.params["force"]:
        create(module.params["base"], auth, dashboard)
    module.exit_json(**result)
