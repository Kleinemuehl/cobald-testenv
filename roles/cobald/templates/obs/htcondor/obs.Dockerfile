FROM {{ base }}
COPY 35-htcondor-cobald-token /etc/shell-init.d/35-htcondor-cobald-token
RUN chmod 755 /etc/shell-init.d/35-htcondor-cobald-token
