FROM {{ _cobald_site_docker_base_image }}

RUN yum update -y && \
    yum install -y python3 git && pip3 install --upgrade pip && \
    yum clean all && rm -rf /var/cache/yum

ARG REPOCOBALD=https://github.com/MatterMiners/cobald
ARG REPOTARDIS=https://github.com/MatterMiners/tardis

RUN git clone $REPOCOBALD /usr/local/src/cobald && \
    git clone $REPOTARDIS /usr/local/src/tardis

RUN mkdir /etc/cobald /var/log/cobald && \
    ( getent passwd cobald > /dev/null || \
        useradd -m -d /var/lib/cobald --no-log-init --system cobald ) && \
    chown cobald:cobald /var/log/cobald

#RUN mkdir /cobald && python3 -m venv /cobald && source /cobald/bin/activate &&\
#	pip3 install --upgrade pip && pip3 install cobald

RUN mkdir /usr/local/src/cobaldmodules /usr/local/lib/cobaldmodules && \
    ln -s /usr/local/src/cobaldmodules/setup.py \
          /usr/local/lib/cobaldmodules/setup.py && \
    ln -s /usr/local/src/cobaldmodules/cobaldmodules \
          /usr/local/lib/cobaldmodules/cobaldmodules && \
    chown cobald:cobald /usr/local/lib/cobaldmodules

RUN pip3 install --editable /usr/local/src/cobald  && \
    pip3 install --editable /usr/local/src/cobald[contrib]

RUN pip3 install --editable /usr/local/src/tardis&& \
    pip3 install --editable /usr/local/src/tardis[contrib]

ENV PYTHONPATH=/usr/local/src/cobaldmodules

# pip3 install --editable .
# pip3 install --editable .[contrib]
# pip3 install --upgrade --editable /etc/cobald/modules/
# su cobald -c "python3 -m cobald.daemon /etc/cobald/config.yaml"

VOLUME /usr/local/src/cobaldmodules

VOLUME /etc/cobald

RUN mkdir -p /usr/local/lib/entrypoints.d/

COPY init-cobaldmodules.sh /usr/local/lib/entrypoints.d/50-init-cobaldmodules.sh

RUN chmod 755 /usr/local/lib/entrypoints.d/50-init-cobaldmodules.sh

COPY 70-cobald /etc/shell-init.d/70-cobald
RUN chmod 755 /etc/shell-init.d/70-cobald

{% for i in _cobald_site_extra_files | default([]) -%}
COPY --chown=root:root {{i.file}} {{i.image_path}}
{% endfor %}
{% for i in _cobald_site_extra_files | default([]) |
			selectattr('mode','defined') -%}
RUN chmod {{i.mode}} {{i.image_path}}
{% endfor %}

# drone sync runs as root and requires known_hosts as well
RUN ln -s /etc/cobald/known_hosts /etc/ssh/ssh_known_hosts

#{% if cobald_user != 'root' %}
#USER {{ cobald_user }}
#
#RUN mkdir ~/.ssh && ln -s /etc/cobald/known_hosts ~/.ssh/known_hosts
#
## user has always to be root to enable startup of daemons
#USER root
#{% endif %}
