# from cobald.interfaces import Pool
from cobald.interfaces import PoolDecorator
from cobald.daemon import service
from tardis.configuration.configuration import Configuration
import asyncio
import aiotelegraf
import platform
import logging
from typing import Optional, Dict, Union


@service(flavour=asyncio)
class TelegrafPipelineMonitor(PoolDecorator):
    @property
    def demand(self):
        return self.target.demand

    @demand.setter
    def demand(self, value):
        if not self.poll:
            self.data.update({
                'demand': float(value), 'supply': float(self.supply),
                'allocation': float(self.allocation),
                'utilisation': float(self.utilisation)
            })
            if self.eventloop:
                self.eventloop.create_task(self.send(self.data))
        self.target.demand = value

    def __init__(self, target, interval: Union[float, int] = 1.0,
                 poll: bool = True, metric: str = "tardis_pipeline",
                 name: Optional[str] = None):
        super().__init__(target=target)
        self.interval: float = float(interval)
#        super().__setattr__("interval", interval)
        # copied from TelegrafMonitoring
        telegraf_cfg = Configuration().Plugins.TelegrafMonitoring
        default_tags = dict(tardis_machine_name=platform.node())
        default_tags.update(getattr(telegraf_cfg, "default_tags", {}))
        self.client = aiotelegraf.Client(
            host=telegraf_cfg.host, port=telegraf_cfg.port, tags=default_tags)
        self.metric: str = metric
        self.tags: Dict[str, str] = dict()
        if name:
            self.tags["pipeline_name"] = name
        self.poll: bool = poll
        self.data: Dict[str, float] = {}
        self.eventloop: Optional[asyncio.AbstractEventLoop] = None

    async def send(self, data: Dict[str, float]):
        try:
            await self.client.connect()
            if data:
                self.client.metric(self.metric, data, tags=self.tags)
            await self.client.close()
        except Exception as e:
            logging.warn(f"sending pipeline data to telegraf failed: {str(e)}")

    async def run(self):
        if not self.poll:
            self.eventloop = asyncio.get_event_loop()
            if self.data:
                self.eventloop.create_task(self.send(self.data))
            return
        while True:
            await asyncio.sleep(self.interval)
            data = dict(
                demand=float(self.target.demand),
                supply=float(self.target.supply),
                allocation=float(self.target.allocation),
                utilisation=float(self.target.utilisation)
                ) if self.poll else self.data
            await self.send(data)
