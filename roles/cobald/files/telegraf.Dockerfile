FROM centos:7

COPY influxdb.repo /etc/yum.repos.d/influxdb.repo

RUN yum -y install telegraf &&\
    yum clean all && rm -rf /var/cache/yum

CMD telegraf
