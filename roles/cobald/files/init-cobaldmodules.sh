#!/bin/sh

[ -f /usr/local/lib/cobaldmodules/setup.py -a \
	-d /usr/local/lib/cobaldmodules/cobaldmodules ] && \
		pip3 install --no-deps --editable /usr/local/lib/cobaldmodules
