# Role cobald user documentation

This role sets up a container with COBalD-TARDIS including site and overlay
batch system configuration and monitoring containers for the COBalD pipeline
and tardis drone status utilizing telegraf and grafana.

## Variables

Most variables have sane defaults and are __described in the
[argument spec](meta/argument_specs.yml).__

For configuration of the batch system and site certain variables are
required, which are facts exported by the corresponding roles, e.g. the
`slurm_testenv` and `htcondor_testenv` roles. This dependency is dynamic
and not enforced via role meta definitions.

Supported sites: `fake` and `slurm`.

Supported overlay batch systems: `fake`, `htcondor`. `slurm` is under
development and moved to branch `obs-slurm`.

## Entrypoints

### main

The main (i.e. default) entrypoint provides a complete build of COBalD-TARDIS.

### facts

The facts entrypoint only retrieves and exports the
`cobald_container_hostname` fact.

## graphing

Grafana runs a HTTP Interface on port 3000. A pair of default credentials
is set on both InfluxDB and Grafana: `my-user` and `my-password`. This
is configurable through the variables `influx_admin_user` and
`influx_admin_pw`. Note that you should change these defaults because
both instances may be reachable from the internet.

In Grafana click the "Dashboards" Button and "Manage" on the left side and
select "cobald". It is possible to change the dashboard view, but changes
will be lost when the playbook is run again.

On the "nodes running" plot there may be negative numbers, which means drones
have failed or gone without reaching the final "down" state. This plot is just
a summation of all drones gone available minus the ones gone down. The
"pipeline" plots rely on the metrics delivered by a pipline monitor, like the
`TelegrafPipelineMonitor` COBalD decorator plugin supplied within this
role.

## TARDIS drone startup

Drones will be started using some kind of batch job start facility, like a
JDL file or a start script in /usr/local/bin/start-drone. Which
file is placed in the container image depends on the type of site.
The job script starts a script running a singularity image placed on the
shared storage
partition shared between execute nodes of the site. It is run by TARDIS
as a job of the site being executed at an arbitraty node of the job
allocation. For example the script is run using sbatch on
slurm sites, typically starting on the first node in the allocation.

Job parameters like job ressources (number of CPU cores or memory limits)
and job run time are passed to the start script and are transformed to
uniform environment variables to be supplied to the container running
inside of the job. Environment variables are passed explicitly to the
singularity container, so they do not inherit any undesired environment
variables from the site node. A node always gets a random UUID for test
reasons if no UUID is given.

Despite having a run time specified, drones may be drained and stopped
earlier by TARDIS. This is done by stopping the job. Drone shutdown is
handled using a bash EXIT trap, i.e. bound to the usual termination signals,
so the start script takes care of this.

Drones can be run with setuid capability to allow for the OBS to run jobs
as the user who submitted them. To allow for this but still not require
running the container as root, fakeroot functionality using user namespaces
is used. This may be switched off by disabling `cobald_drone_userns`.
Ability to run with user namespaces is discovered on drone startup and the
drone container is run with or without user namespaces automatically. Drones
running as root are also detected and handled accordingly. Note,
that the mapped user IDs inside the container are different from the site
node and may have issues using shared filesystems. The
user running the drone is configured by `cobald_user` and
`cobald_site_ssh_user`. To enable running as non-root without user namespaces
it is necessary to set `cobald_drone_public_secrets=True` to enable
authentication against in OBS.

Overlay batchsystems running inside the drone container require having a
writable filesystem path to write runtime data to, e.g. state files and
job data. These data may be large or require not to run on a tmpfs
filesystem. Therefore a path on the site nodes filesystem is bind-mounted
inside the drone container. The drone containers configuration should
adjust paths to end up inside this location or be symlinked there. There
may be additional mounts for shared filesystems using the
`cobald_drone_bindmounts` list variable.
These will be mounted in the same place in the
drone filesystem.

Note that drone singularity images are unencrypted and contain secrets
providing access to the overlay batch system. Therefore special care should
be taken to ensure correct permissions on the shared filesytem.

Additionally, when setting `cobald_drone_public_secrets=True`, which is
necessary to run as non-root user without user namespaces, the secrets
necessary to join the cluster are readable by any user of the OBS cluster.
In the same scenario, all jobs will run as user `cobald` and have access
to each others data and are able to kill or replace the OBS control daemons.
This means access should only be permitted to trusted users when disabling
user namespaces.

### slurm site drones

The start script on slurm sites is always run using sbatch and is executed on
one of the execute nodes attributed to the job. The sbatch script executes
a drone script using srun which starts
the singularity containers on the execute nodes. Therefore it is
possible to start multiple drones at once, which is not implemented by
TARDIS yet. Resource definitions like the number of CPUs in one drone is
passed using sbatch job description arguments, while some parameters like
the drone UUID are passed using environment variables.

It is
possible to deliver additional singularity command line arguments to the
drone container using the `MORE_ARGS`
environment variable. The role variable `cobald_drone_bindmounts`
can be used to provide additional mounts
from the execute nodes filesystem.

Shutdown time, kill time and latencies are considered in the drones run
time calculation. This is done using the following chart:

```
JOB_T_START                    DRONE_T_SHUTDOWN        JOB_T_KILL
v        DRONE_T_START               v        DRONE_T_KILL v  JOB_T_END
v             v                      v                 v   v     v
/-------------------------SLURM_Walltime-------------------------/  <= slurm
|             v                      v                 v   v     |
/L/-TD_SYNC-/L/-----------------JOB_TD_RUN-----------------/L/K/L/  <= job
              |                      v                 v   |
              /-----DRONE_TD_RUN-----/L/--TD_SHUTDOWN--/L/K/        <= drone

L = TD_LATENCY
K = TD_KILL
DRONE_TD_SHUTDOWN ~= TD_SHUTDOWN
JOB_T_END = T_END, JOB_T_START = T_START
```

The time when the job is killed by slurm, slurm EndTime or `JOB_T_END` is
read inside the running job through `scontrol show job`. From this point
all times where events should be started, like shutdown of the OBS, are
calculated. At shutdown time (`DRONE_T_SHUTDOWN`) a graceful shutdown is
initiated sending
`SIGUSR1` to the container. After the shutdown time duration (`TD_SHUTDOWN`)
is due, the container is terminated using `SIGTERM` on the drone script side
(`DRONE_T_KILL`). This usually causes a fast shutdown of the OBS killing all
jobs immediately. This fast shutdown is also triggered immediately when the
job is canceled using scancel, which may also be caused by TARDIS shuting
down drones no longer necessary. Note that this means termination grace time
is only considered on planned drone shutdowns, i.e. after job runtime is over
and not when TARDIS shuts drones down. Afterwards the job is killed by the
`sbatch` script (at `JOB_T_KILL`). After terminating the drone and the job,
leftover files are cleaned up.

In the picture above there is a sync time, which does not happen at the
moment since the drone image and drone script are already present on the
cluster since a shared filesystem is used. This time would be used to pull
drone images and startup scripts in future implementations. At the moment
drone container images are always copied to and updated in shared storage
at startup of the cobald container.

Note that due to shutdown time being calculated from job end time, RTC time
between nodes have to be synchronized for correct operation.

Shutdown time in seconds may be adjusted by changing the variable
`cobald_drone_shutdown_time` (default 30s). Also the latency
`cobald_drone_latency` (default 2s) must be fit to the site, matching the
time it takes to finish operations like starting a container or removing
files. Kill time, `cobald_kill_time` (default 2s), should be matched
to how long it takes to perform a fast shutdown of a OBS node.

### drone configuration

Usually drone containers need some changes to the default cluster
configuration to run. This is the case for multiple reasons, for example:
* the drone terminates at a certain point in time, so the cluster node
	has to shut down.
* resources of the job, e.g. CPUs or memory, are limited and not reflected
	by the ressources which appear to be available from the containered
	applications point of view. Therefore the auto detection of the drone
	ressources does not work and has to be specified matching the
	underlying job description.
* running inside a container may be different from running inside a typical
	node system. Software may not be available as easily or the filesystem
	of the container may be readonly apart from certain directories. Therefore
	storage of job runtime data, e.g. the job executable, has to be adjusted
	to a writable path.
* nesting cgroups may not work as expected, especially when using cgroups v1.
	For more information see [the section about htcondor obs
	below](#htcondor_tuning_for_drone_usage)

### htcondor tuning for drone usage

To run htcondor inside a drone multiple adjustments are necessary.
Parameters from the job starting the drone are imported in the
static htcondor configuration using environment variables.

Cgroups are not tested in depth, so cgroups should be disabled.
For testing cgroups can be enabled by setting `_cobald_mount_cgroups=True`.
Mounting
the cgroup the singularity container is running in inside of it at
`/sys/fs/cgroup` may not work quite right, because there is a
discrepancy between paths in
`/proc/self/cgroups` and `/sys/fs/cgroup`.
Disabling cgroups by using
`BASE_CGROUP=` inside htcondor configuration is a good idea
for testing, but maybe it is not a good idea to do this in a production
environment.

The drone presents one partitionable slot to the cluster, matching the
resource limits of the sites job. There is no hard limiting enforced from
the drone side, but the site batch system may do so using cgroups.

The default domain name should be set, because containers ran by singularity
usually do not set it.

Drone attributes are exported using attributes of startd (`STARTD_ATTRS`).
This includes the UUID of the drone (`TardisDroneUuid` ClassAd).

Filesystem paths have to be writable and therefore need to be adjusted to a
writable part of the filesystem which is bind-mounted from the underlying
host system. This includes paths for scheduling, logging and the job
executable and must not be a tmpfs.

Due to the site having allocated ports for the underlying batch system and
multiple drones running on one site node, the ports for the overlay batch
system can not be static but must be assigned dynamically. This is done using
the shared port daemon feature of htcondor and setting `SHARED_PORT_PORT=0`
to avoid using the default, static port 9618.

A rank calculation is introduced to sanction starting jobs exceeding the
remaining drone runtime.

In order to allow for a unprivileged user without access to user namespaces
to run htcondor daemons and join the overlay cluster, the `cobald` user
is added to the `condor` group running the daemon and read permission on
the hosts idtoken is set to include the group `cobald`.

There are some security implications when running htcondor as non-root user:
When run without setuid permissions, i.e. without root and user namespaces,
jobs all run as the same user and all gain access to the htcondor idtoken to
join the cluster.

Also, since there is no option yet to generate a idtoken on demand, all tokens
are shared between drones and have infinite lifetime. There are options in
htcondor which would allow to limit this.

## docker image layers

The docker image for the container in which COBalD-TARDIS will be running,
is built from multiple layers. The base image is a docker image being able
to start jobs of the site. The second image layer contains the COBalD-TARDIS
software. The last layer adds the OBS part.

## description of operation

First, this role sets up variables for site and obs. Afterwards the
drone image build using singularity is started asynchronously if necessary.
Build files
are placed in `{{ cobald_builddir }}/obs-.../`. The resulting drone
file is placed in subfolder `build`. Drone images are copied to the shared
cluster partition on cobald container startup.
After starting drone build the
COBalD image build including site and obs is started using the role
specified in variables for the obs part of the image.

The configuration is generated from the template and placed in the
specified directory. Static configuration like COBalD plugins are copied
as well. There is always a subdirectory called
modules to facilitate providing a read-only volume for COBalD plugins,
which supports installation via pip. The installation of these modules
is done in an entrypoint script of the COBalD container at start time.

Installation of COBalD and TARDIS software using pip is done at image build
time as well as on playbook run. This enables both deploying a stable version
and modified source code without rebuilding of the image. The installation
via pip of both installation methods happens at build time and using a
temporary cobald docker container at playbook run time respectively.

Telegraf monitoring is set up and configured. Configuration data for the
monitoring setup is also placed in directories specified by variables
`telegraf_config_dir` and `cobald_builddir`.
Telegraf is built on top of a CentOS7 image, InfluxDB v2 and Grafana are
pulled as binary docker images from Docker Hub. The Telegraf container is
named `ed-telegraf` and this name is used as connection target by the cobald
configurtion template. InfluxDB v2 is started first, telegraf second and
Grafana as last container.

To be able to configure InfluxDB v2, an ansible connection of type docker is
used to run modules inside the InfluxDB container in order to get the initial
authentication token. Since Ansible only provides modules for version 1,
a set of custom Ansible modules for InfluxDB v2 is used to set up a
bucket, generate a authentication token for telegraf and upload a InfluxDB
dashboard. The Grafana dashboard should be preferred over the influxdb
dashboard. The Grafana instance has its own read-only access token
to influxdb.

After finishing the drone image build, the cobald container is defined
and started if requested. The container container hostname is randomly
generated but persistent across playbook runs. This is necessary to enable
graphing to calculate the correct number of nodes.

# development documentation

Internal variables are generated automatically and not to be modified or used
by external roles or through user configuration. They are prefixed with an
underscore (`_`). Definitions for or by the site and batch system
configuration are usually prefixed by `_cobald_site` and `_cobald_obs`
respectively.

## internal variables

These variables are only to be used internally and never to be specified
as role parameters or relied on to exist after the role ran.

### general internal variables

* `_cobald_obs_mounts`, `_cobald_site_mounts`: (docker) volumes required for
	site and obs to work inside the container. Additionally the configuration
	directory for cobald is passed too (`_cobald_mounts`).
* `_cobald_container_info`: docker container defintion of existing cobald
	container. Used to extract old container instances hostname. (see facts)
* `_cobald_site_docker_base_image`: base image to built cobald image upon.
	Usually
	this is the site docker image. This image has to provide the shell-init
	as startup system for spawning processes.
* `_cobald_docker_default_command`: when using fake site and obs there is no
	need to run as root. This makes cobald run as non-root and starts cobald
	directly without any wrappers.
* `_cobald_image_tag`: tag for the docker image, being site-obs or site.
	Image name is always cobald.
* `_cobald_obs_image_role`, `_cobald_obs_image_role_file`: name and entrypoint
	of the role which builds the obs docker image. Passes `image` and `base`
	variable.
* `_cobald_cobald_git_pull` and `_cobald_tardis_git_pull`: registered results
	from git pull operation of source code
* `_cobald_site_extra_files`: dict like in
	[docker roles image file specification](../docker/README.md#variable-on-image-entrypoint)
	extended by the following attributes:
	* `image_path`: place in the final docker image where this file should be
	* `bindmount`: optional bind mount from the docker host to enable editing.
		This enables modification of files without rebuilding the container
		image but just restarting the container. Note that, due to
		implementation of bind mounts, files do not get updated live inside
		the container.
* `_cobald_default_docker_base_image`: default base image to use for drones
	if no site specific image is used, defaults to `docker_shinit_image`.
* `_cobald_site_tag`, `_cobald_image_tag`: docker image tag for site image and
	complete cobald image.
* `_cobald_site_role`: role to use to add ssh keys using its `ssh_addkey`
	entrypoint.
* `_cobald_site_variant`: method of starting drone jobs: ssh or sitenode. Used
	to include matching variable files.
* `_cobald_site_assertions`, `_cobald_obs_assertions`: assertions to provide
	early failure in the role if parameters are invalid or missing.
* `_cobald_drone_sync`, `_cobald_drone_sync_common`, `_cobald_drone_sync_ssh`:
	file to sync from the running container to the shared filesystem or the
	submit node.
* `_cobald_homes`, `_cobald_tilde`, `_cobald_ssh_lib_path`: unused, expansion
	of `~` character to home directory.
* `_cobald_site_user` user that most likely runs the drone

### drone internal variables

* `_cobald_obs_base`: docker base image of the drone singularity container image
* `_cobald_obs_build_drone`: build drone when real overlay batch system is used
	(i.e. `cobald_obs` is not `fake`)
* `_cobald_obs_rebuild`: triggers rebuild of singularity drone image when
	docker obs image or configuration changes

## default docker images

* fake site: `docker_shinit_image` (variable)
* slurm site: `slurm.base_image` (variable)
* slurm obs: `slurm:slurmd`
* htcondor obs: `htcondor:latest`
	image role: htcondor_testenv, entrypoint image
* fake obs: none
* drones: `_cobald_default_docker_base_image`

## Tags
* `cobald_config`: when skipped, does not update configuration and modules in
	/etc/cobald/
* `cobald_monitoring`: when skipped, does not set up monitoring
* `cobald_build_drone`: when skipped, does not build drone image
* `cobald_build_container`: when skipped, does not build container images
* `cobald_devenv`: when skipped, does not update local cobald tardis source

## Examples

```
- hosts: cobald
  vars:
	cobald_site: fake
	cobald_obs: fake
    cobald_site_config:
      name: mysite
      mtypes:
      - name: m1.a
        Walltime: 30
        Cores: 1
        Memory: 1.5
        Disk: 4
    cobald_config_dir: "~{{unpriv_user}}/cobald/"
    cobald_container_name: cobald

  tasks:
  - include_role:
      name: cobald
      tasks_from: facts
  - debug: msg="{{ cobald_container_hostname }}"
  - include_role:
      name: cobald
```


# Issues

1. /etc/cobald should be mounted readonly. This is not possible because the ssh
	key permissions need to be adjusted.
1. There is just one working combination of site and obs: slurm site and
	htcondor obs. Slurm OBS are not working yet and htcondor sites are
	not even implemented.
1. No encryption of drones containing secrets like obs keys
