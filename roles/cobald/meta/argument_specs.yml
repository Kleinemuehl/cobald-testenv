argument_specs:
  facts:
    short_description: gets cobald_container_hostname
    description: Gathers hostname of cobald container. When no cobald container
      exists yet, a new random hostname is generated.
      Exported fact cobald_container_hostname
    author:
    - Thorsten Mueller
    options:
      cobald_container_name:
        description: hostname of the container
        required: True
  main:
    short_description: sets up cobald-tardis
    description: Sets up a container with cobald-tardis including site and
      overlay batch system configuration and monitoring containers for the
      cobald pipeline and tardis drone status utilizing telegraf and grafana.
      Exports container hostname like facts entrypoint.
    author:
    - Thorsten Mueller
    options:
      cobald_container_name:
        description: Name of cobald docker container. This is not equal to the
          containers hostname, which is generated randomly to ensure monitoring
          data consistency.
        default: cobald
      cobald_domainname:
        description: Domain name for cobald container. This may be overwritten
          to the sites domain name by the cobald site module.
        default: cobald.local
      cobald_site:
        description: Condor site type. Used to determine base image for
          cobald docker image. If not fake, the corresponding role, must be
          executed before running this role.
        choices: [ fake, slurm ]
        default: fake
      cobald_obs:
        description: Condor overlay batch system type. Used to build the
          the final stage for the cobald docker image. If not fake, the
          corresponding role must be executed before running this role.
        choices: [ fake, slurm, htcondor ]
        default: fake
      cobald_drone_bindmounts:
        description: list of bind mounts to be mounted in drone
        type: list
        elements: str
        required: False
      cobald_docker_network:
        description: docker network used to connect to site and obs. The site
          configuration overrides this variable.
        default: bridge
      cobald_config_dir:
        description: directory with configuration and modules and other data
          for cobald tardis
        default: "{{ cobald_container_data_dir }}/cobald/"
      cobald_sharedfs_path:
        description: path on the shared filesystem of the cluster where cobald
          puts its files
        default: /shared/cobald/
      cobald_user:
        description: user to run the cobald daemon.
        default: root
      cobald_start:
        description: Start cobald container when running playbook. This is
          useful to prevent cobald from running drones when testing drone
          configurations. When disabled a container is still built so
          `--volumes-from` works.
        default: True
      cobald_config_overwrite:
        description: update configuration files of cobald and tardis
        default: True
      cobald_build_images:
        description: like role docker image variable `do_build`. Disable
          building image setting `do_build = False`. Force building using
          `do_build = force`. Handles drones as well as cobald container image.
        default: True
        choices: [ True, False, force ]
      cobald_builddir:
        description: path where drone images will be built
        default: "{{ docker_builddir }}"
      docker_builddir:
        description: like in role docker entrypoint image. Path where
          containers will be built.
        required: True
      cobald_container_data_dir:
        description: shortcut
        default: "/container/volumes"
      cobald_telegraf_config_dir:
        description: telegraf configuration location
        default: "{{ cobald_container_data_dir }}/telegraf/"
      cobald_influx_backup_dir:
        description: influxdb backup volume location
        default: "{{ cobald_container_data_dir }}/influxdb-backup/"
      cobald_influx_hostname:
        description: hostname of influxdb container. Note the container is
          always named ed-influxdb
        default: ed-influxdb
      cobald_drone_runtime_path_prefix:
        description: path of the drone writable mounts on the site node
        default: /tmp/drone_var_
      cobald_drone_log_path_prefix:
        description: path of the drone log directory on the site node
        default: /tmp/drone_log_
      cobald_drone_job:
        description: place of the drone job in the cobald container
        default: /usr/local/lib/cobald/drone_job
      cobald_drone_job_ssh:
        description: place of the drone job on the ssh submit node
        default: "{{ cobald_ssh_lib_path }}/drone_job"
      cobald_drone_script:
        description: place of the drone execution script on site nodes
        default: "{{ cobald_sharedfs_path }}/drone_run"
      cobald_drone_image:
        description: place of the drone image on site nodes
        default: "{{ cobald_sharedfs_path }}/drone.sif"
      cobald_drone_public_secrets:
        description: make configuration secrets in drones publicly readable.
          This is necessary to run drones rootless and without user namespaces.
        default: False
      cobald_ssh_keytype:
        description: type of the ssh client key
        default: ed25519
      cobald_site_ssh:
        description: hostname of the ssh node where drones should be started
          remotely. If falsy ssh job starts will be disabled and drone jobs
          will be started directly, making the cobald-tardis container to a
          node in the site cluster.
        required: False
      cobald_site_ssh_user:
        description: user on ssh submit node
        default: cobald_user
      cobald_ssh_lib_path:
        description: path on submit node where job files will be placed.
        default: ~/cobald
      cobald_drone_shutdown_time:
        description: OBS job shutdown time in seconds
        default: 30
      cobald_drone_kill_time:
        description: OBS job fast shutdown (kill) time in seconds
        default: 2
      cobald_drone_latency:
        description: maximum latency of all job operations (signals,
          communication, etc.) in seconds
        default: 2
      _cobald_mount_cgroups:
        description: use cgroups inside obs
        default: False
      cobald_drone_userns:
        description: use user namespaces inside drone
        default: True
      cobald_drone_obs_htcondor_runtimeconfig:
        description: path to optional dynamic configuration for htcondor obs
        required: False
      influxdb_admin_user:
        description: influxdb admin user name
        default: my-user
      influxdb_admin_pw:
        description: influxdb admin password
        default: my-password
      influx_org:
        description: influxdb default organization
        default: my-org
      influx_bucket:
        description: influxdb data bucket to log cobald data into
        default: batleth
      influx_pubport:
        description: influxdb port to be accessed from outside host
        default: 28086
      unpriv_user:
        description: unprivileged user to own configuration files to
        required: True
