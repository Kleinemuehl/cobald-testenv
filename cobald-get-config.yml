---
- hosts: cobald
  vars:
    slurm_ssh_submit_host: "{{ groups['slurm_ssh_submit'] | first }}"
  vars_files:
  - vars/slurm-ssh-site-htcondor-obs-common.yml
  debugger: on_failed
  #debugger: always
  tasks:
  # this role initializes container build stuff like dependency management.
  # therefore docker is not installed (docker_install=False) or set up in any
  # way but just the preparations for the common shinit base image is done
  - name: "setup docker"
    import_role: name=docker
    vars:
      docker_install: False
    tags: docker

  - name: setup singularity on site exec nodes
    include_role:
      name: singularity
      apply:
        tags: singularity
        delegate_to: "{{ host }}"
    vars:
      userns_user: "{{ site_user }}"
    when: use_userns | mandatory
    loop: "{{ groups['slurm_nodes'] }}"
    loop_control:
      loop_var: host
    tags: singularity, singularity_nodes

  - name: "get facts from existing cobald instance (i.e. hostname)"
    include_role:
      name: cobald
      tasks_from: facts
      apply:
        tags: slurm, cobald, slurm-config
    tags: slurm, cobald, slurm-config

  - name: "get facts from slurm"
    include_role:
      name: slurm_facts
      apply:
        tags: slurm
    vars:
      slurm_docker_network: "{{docker_network}}"
    tags: slurm, cobald, influxdb, slurm-config
    # tags: cobald requires some slurm facts, so cobald tag is included here

  - name: "setup htcondor obs in docker containers"
    include_role:
      name: htcondor_testenv
      apply:
        tags: htcondor
    vars:
      htcondor_container_privileged: True
      htcondor_docker_network: "{{docker_network}}"
      htcondor_domain: htc.local
      htcondor_num_exec: 0
      htcondor_extra_volumes:
      - "slurm-shared:/shared/:rw"
      htcondor_config_path: /container/htcondor
    tags: htcondor, htcondor_docker_image, cobald

  - name: "install cobald"
    include_role:
      name: cobald
      apply:
        tags: cobald
    vars:
      cobald_site: slurm
      cobald_obs: htcondor
      cobald_container_name: cobald
      cobald_drone_bindmounts:
      - "/shared/"
      cobald_site_ssh: slurm-sshsub
      cobald_site_ssh_user: "{{ site_user }}"
      cobald_user: cobald
      cobald_site_config:
        name: slurmtest
        mtypes:
        - name: m1.a
          Walltime: 30 # minutes
          Cores: 1 # cores via -n
          Memory: 1.5 # gb
          Disk: 4 # only passed via env, not evaluated yet
        - name: m1.b
          Walltime: 30
          Cores: 1
          Memory: 0.5
          Disk: 4
      cobald_drone_public_secrets: True
      htcondor_drone_config_path: "{{ htcondor.exec.config }}"
      htcondor_submit_config_mounts: "{{ htcondor.sub.mounts }}"
    when: '"cobald" in group_names'
    tags: cobald, influxdb, singularity
